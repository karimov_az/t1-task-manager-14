package ru.t1.karimov.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void updateTaskById();

    void updateTaskByIndex();

    void showTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void clearTasks();

}
