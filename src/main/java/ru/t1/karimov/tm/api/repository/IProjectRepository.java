package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project create(String name, String description);

    Project create(String name);

    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    boolean existsById(String id);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

}
