package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.enumerated.Sort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

    Task create(String name);

    Task add(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    void remove(Task task);

    boolean existsById(String id);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeTaskStatusById (String id, Status status);

    Task changeTaskStatusByIndex (Integer index, Status status);

    int getSize();

}
