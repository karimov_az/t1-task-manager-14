package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    void clear();

    Task create(String name, String description);

    Task create(String name);

    boolean existsById(String id);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}
