package ru.t1.karimov.tm.api.controller;

public interface IProjectController {

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void showProjects();

}
